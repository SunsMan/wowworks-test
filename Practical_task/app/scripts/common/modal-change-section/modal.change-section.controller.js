modalChangeSectionModule.controller('ModalChangeSectionController', ['$scope', '$q', '$interval', 'ServicesKeeper', 'AssignmentsKeeper',
    function($scope, $q, $interval, ServicesKeeper, $modalInstance, section, AssignmentsKeeper) {

        $scope.sections = {};
        $scope.sections.selected = "";

        if (section != "unknown")
            $scope.sections.selected = section;
        else
            $scope.sections.selected = "it";
        $scope.sections.category = 0;

        $scope.sections.services = [];
        $scope.exitInfo = {};
        $scope.sections.hadChoosenAnother = false;
        $scope.sections.hadChoosenNew = false;

        $scope.changeSection = function() {
            $scope.exitInfo.clearBlock = true;
            $scope.sections.hadChoosenNew = true;
            $scope.sections.hadChoosenAnother = false;
            $scope.sections.selected = $scope.sections.markedSection
            $scope.sections.services = ServicesKeeper.getService();
        }

        $scope.selectService = function(serviceJSON) {
            var result = AssignmentsKeeper.getServiceInstFromJSON(serviceJSON);
            $scope.exitInfo.section = markedSection;

            $modalInstance.close(result, $scope.exitInfo); //ToDo   must sent service-object
        };

        $scope.close = function() {
            $modalInstance.dismiss();
        };

        $scope.cancel = function() {
            section.hadChoosenAnother = false;
        }
    }
]);
