modalAddServiceModule.controller('ModalAddServiceController', ['$scope', '$q', 'ServicesKeeper', 'AssignmentsKeeper', '$modalInstance', 'section',
    function($scope, $q, ServicesKeeper, AssignmentsKeeper, $modalInstance, section) {

        $scope.sections = {};
        $scope.sections.selected = "";

        if (section != "unknown") {

            $scope.sections.selected = section;
            ServicesKeeper.getServices($scope.sections.selected).then(function(data) {
                $scope.sections.services = data;    
            });
        }
        else {
            $scope.sections.selected = "it";
            ServicesKeeper.getServices($scope.sections.selected).then(function(data) {
                $scope.sections.services = data;    
            });
        }
        $scope.sections.category = 0;

        $scope.sections.services = [];
        $scope.exitInfo = {};
        $scope.exitInfo.clearBlock = false;
        $scope.sections.hadChoosenAnother = false;
        $scope.sections.hadChoosenNew = false;

        $scope.changeSection = function() {
            $scope.exitInfo.clearBlock = true;
            $scope.sections.hadChoosenNew = true;
            $scope.sections.hadChoosenAnother = false;
            $scope.sections.selected = $scope.sections.markedSection
            $scope.sections.services = ServicesKeeper.getService();
        }

        $scope.markAsSelected = function(markedSection) {
            // var modalInstance = $modal.open({
            //     animation: $scope.animationsEnabled,
            //     templateUrl: '/partials/modal_change_section.html',
            //     controller: 'ModalChangeSectionController',
            //     size: 'md',
            //     backdropClass: "fade",
            //     resolve: {
            //         section: function() {
            //             // send section of the selected block; mb so?
            //             return $scope.blockList[$scope.selectedBlock].getSection();
            //         }
            //     }
            // });

            // modalInstance.result.then(function(serviceObj, exiteInfo) {
            //     // add to selected block returned service
            //     if (exiteInfo.clearBlock) {
            //         $scope.blockList[$scope.selectedBlock].clearServiceList();
            //     }

            //     if ($scope.blockList[$scope.selectdBlock].getSection() != exiteInfo.section) {
            //         $scope.blockList[$scope.selectedBlock].setSection(exiteInfo.section);
            //     }
            //     $scope.blockList.areWeHaveAddedService = true;
            //     $scope.blockList.summaryDuration += serviceObj.getDuration();
            //     $scope.blockList.summaryCost += serviceObj.getCost();

            //     $scope.blockList[$scope.selectedBlock].setSection(exiteInfo.section);

            //     $scope.blockList[$scope.selectedBlock].addService(serviceObj);

            // }, function() {
            //     // Are we really need todo smth?
            // });

            // $scope.sections.markedSection = markedSection;
            // if (!$scope.sections.hadChoosenNew) {
            //     if (section != "unknown" && markedSection != section) {
            //         $scope.sections.hadChoosenAnother = true;
            //         // change content of this modal
            //         // some operations with DOM - bad style but real usefull in this case
            //     } else {
            //         $scope.sections.selected = markedSection;
            //         $scope.sections.services = ServicesKeeper.getService($scope.sections.selected);
            //         $scope.sections.hadChoosenAnother = false;
            //     }
            // } else {
            //     $scope.sections.selected = markedSection;
            //     $scope.sections.services = ServicesKeeper.getService($scope.sections.selected);
            //     $scope.sections.hadChoosenAnother = false;
            // }
            $scope.sections.selected = markedSection;
            ServicesKeeper.getServices($scope.sections.selected).then(function(data) {
                $scope.sections.services = data;    
            });

        }
        $scope.getCategoryNumber = function(value) {
            return parseInt(value);
        };
        $scope.selectService = function(serviceJSON) {
            var result = AssignmentsKeeper.getServiceInstFromJSON(serviceJSON);
            $scope.exitInfo.section = $scope.sections.selected;
            $scope.exitInfo.serviceObj = result;
            $modalInstance.close($scope.exitInfo); //ToDo   must sent  $modalInstanceser, vice-onect
        };

        $scope.close = function() {
            $modalInstance.dismiss($modalInstance, 'cancen');
        };

        $scope.cancel = function() {
            $scope.section.hadChoosenAnother = false;
        }
    }
]);
