'use strict';

/* App Module */

var app = angular.module('app', [
    'ngRoute',
    'ngResource',
    'ngSanitize',
    'ui.bootstrap',
    'modules'
]);
// login template
// app.config(function($resourceProvider) {
//     $resourceProvider.defaults.stripTrailingSlashes = false;
// });

// app.run(['$rootScope', '$location', '$cookieStore', '$http',
//     function($rootScope, $location, $cookieStore, $http) {
//         // keep user logged in after page refresh
//         $rootScope.globals = $cookieStore.get('globals') || {};
//         if ($rootScope.globals.currentUser) {
//             // $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
//         }

//         $rootScope.$on('$locationChangeStart', function(event, next, current) {
//             // redirect to login page if not logged in
//             if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
//                 $location.path('/login');
//             }
//         });
//     }
// ]);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/', {
            templateUrl: 'partials/create_task.html',
            controller: 'ServiceCreatorController'
        }).
        otherwise({
            redirectTo: '/'
        });
    }
]);

app.controller('appMainController', ['$scope',
    function($scope) {
        
    }
]);

// app.controller('ModalConfigsController',
//     function($scope, $modalInstance, format) {
//         $scope.dateFormatCustom = format;
//         //ConfigsKeeper.
//         $scope.dateFormatSelect = function() {
//             if ($scope.dateFormatCustomSelect)
//                 $scope.dateFormatCustom = $scope.dateFormatCustomSelect.substring(1, $scope.dateFormatCustomSelect.lastIndexOf('"'));
//         };

//         $scope.ok = function() {
//             $modalInstance.close($scope.dateFormatCustom);
//         };

//         $scope.cancel = function() {
//             $modalInstance.dismiss();
//         };
//     }
// );

// app.factory("ConfigsKeeper", ['TimeZoneGETResource', '$q',
//     function(TimeZoneGETResource, $q) {
//         var _dateFormatDefault = "DD-MMMM-YYYY hh:mm:00 A";
//         var _serverTimeZoneDefault = 0;
//         var _dateFormat = _dateFormatDefault;
//         var _serverTimeZone = _serverTimeZoneDefault;
//         var _cookiesHadRead = false;

//         return {
//             get: function() {
//                 return null;
//             }
//         };
//     }
// ]);

// app.factory("TimeZoneGETResource", ["$resource",
//     function($resource) {
//         return $resource(address + "timezone", {}, {
//             query: {
//                 method: "GET"
//             }
//         });
//     }
// ]);
