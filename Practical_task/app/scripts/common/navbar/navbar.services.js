navbarModule.factory("MenuKeeper", 
    function() {
        var bossMenu = [
            {
                "header": {
                    "text": ""
                },
                "elements": [{
                        "href": "http://develop.wowworks.ru/admin/task_list.php",
                        "text": "Задания"
                    },{
                        "href": "http://develop.wowworks.ru/admin/expired_tasks.php",
                        "text": "Просрочки!"
                    },{
                        "href": "http://develop.wowworks.ru/admin/users_isp.php",
                        "text": "Исполнители"
                    },{
                        "href": "http://develop.wowworks.ru/admin/users_org.php",
                        "text": "Пользователи орг."
                    },{
                        "href": "http://develop.wowworks.ru/admin/org.php",
                        "text": "Организации"
                    },{
                        "href": "http://develop.wowworks.ru/admin/comments.php",
                        "text": "Последние комментарии"
                    },{
                        "href": "http://develop.wowworks.ru/admin/user_profile_history.php",
                        "text": "Изменения профиля"
                }]
            },{
                "header": {
                    "text": "Инструменты"
                },
                "elements": [{
                        "href": "http://develop.wowworks.ru/admin/admins.php",
                        "text": "Админы"
                    },{
                        "href": "http://develop.wowworks.ru/admin/skills_accept.phphttp://develop.wowworks.ru/admin/email.php",
                        "text": "Одобрение умений"
                    },{
                        "href": "http://develop.wowworks.ru/admin/email.php",
                        "text": "E-mail рассылка"
                    },{
                        "href": "http://develop.wowworks.ru/admin/sms.php",
                        "text": "SMS рассылка"
                    },{
                        "href": "http://develop.wowworks.ru/admin/category.php",
                        "text": "Категории заявок"
                    },{
                        "href": "http://develop.wowworks.ru/admin/skills.php",
                        "text": "Умения"
                    },{
                        "href": "http://develop.wowworks.ru/admin/coef.php",
                        "text": "Региональный коэффициент"
                    },{
                        "href": "https://hostedredmine.com/projects/wowworks-ru/issues",
                        "text": "Баг-трекер"
                    },{
                        "href": "http://analitics.wowworks.ru/",
                        "text": "Аналитика (admin/Wowworks123)"
                    }
                ]
            },{
                "header": {
                    "text": "Статистика"
                },
                "elements": [{
                        "href": "",
                        "text": "Счета"
                    },{
                        "href": "http://develop.wowworks.ru/admin/acts.php",
                        "text": "Статистика по финансам"
                    },{
                        "href": "http://develop.wowworks.ru/admin/stats_all.php",
                        "text": "Статистика по кураторам"
                    },{
                        "href": "http://develop.wowworks.ru/admin/stats_regions.php",
                        "text": "Статистика по регионам"
                    },{
                        "href": "http://develop.wowworks.ru/admin/stats_category.php",
                        "text": "Статистика по категориям"
                    },{
                        "href": "http://develop.wowworks.ru/admin/extensions.php",
                        "text": "Чеки по допам<"
                    },{
                        "href": "http://develop.wowworks.ru/admin/paymentHistory.php",
                        "text": "Платежи"
                    },{
                        "href": "http://develop.wowworks.ru/admin/addpaymentdocuments.php",
                        "text": "Загрузка платежных документов"
                    },{
                        "href": "http://develop.wowworks.ru/admin/bugtracker.php",
                        "text": "Обратная связь"
                    },{
                        "href": "http://develop.wowworks.ru/admin/stats_days.php",
                        "text": "Счетчики"
                }]
            }
        ];

        var userMenu = [
            {
                "href": "http://develop.wowworks.ru/wallet/balance/",
                "text": "Финансы"
            },{
                "href": "http://develop.wowworks.ru/pc_org/settings.php",
                "text": "Профиль компании"
            },{
                "href": "http://develop.wowworks.ru/pc_org/profile.php",
                "text": "Настройки аккаунта"
            },{
                "href": "http://develop.wowworks.ru/pc_org/settings.php?show=staff",
                "text": "Сотрудники"
            },{
                "href": "http://develop.wowworks.ru/pc_org/settings.php?show=address",
                "text": "Адресная книга"
            },{
                "href": "http://develop.wowworks.ru/pc_org/settings.php?show=acts",
                "text": "Юр.лица"
            },{
                "href": "http://develop.wowworks.ru/wallet/price/",
                "text": "Прайс-лист"
            },{
                "href": "http://develop.wowworks.ru/admin/index.php",
                "text": "[ BOSS ]"
            }
        ];

        return {
            getPreproccessedRepresentation: function(value) {
                switch(value) {
                    case 'BOSS':
                        return bossMenu;
                    case 'userMenu':
                        return userMenu;
                }
            }

        }        
    }
);