navbarModule.controller('NavbarController', ['$scope', '$q', '$interval', 'MenuKeeper',
    function($scope, $q, $interval, MenuKeeper) {

        $scope.dropdownBoss = MenuKeeper.getPreproccessedRepresentation('BOSS');
        $scope.userMenu = MenuKeeper.getPreproccessedRepresentation('userMenu');
        $scope.countNotifications = 0;
        $scope.countMessages = 0;
    }
]);
