var navbarModule = angular.module('navbar', []);
var serviceCreatorModule = angular.module('serviceCreator', ['ui.bootstrap', 'dndLists']);
var modalAddServiceModule = angular.module('modalAddService', ['ui.bootstrap']);
var modalChangeSectionModule = angular.module('modalChangeSection', ['ui.bootstrap']);

var modules = angular.module('modules', [
  'navbar',
  'modalAddService',
  'serviceCreator',
  'modalChangeSection'
]);