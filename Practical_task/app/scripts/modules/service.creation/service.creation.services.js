serviceCreatorModule.factory("AssignmentsKeeper", 
    function() {
    	var Block = (function(){
    		function Block() {
    			this._services = [];
    			this._name = "Блок работ";
    			this._summaryCost = 0;
    			this._summaryDuration = 0;
    			this._section = "unknown";
    		}

    		Block.prototype.addService = function(service) {
                this._summaryCost += service.getCost();
                this._summaryDuration += service.getDuration();
    			this._services.push(service);
    		}
    		Block.prototype.deleteService = function(serviceKey) {
                this._summaryCost -= this._services[serviceKey].getCost()*this._services[serviceKey].getCount();
                this._summaryDuration -= this._services[serviceKey].getDuration()*this._services[serviceKey].getCount();
    			this._services.splice(serviceKey, 1);
    		}
            Block.prototype.getService = function(serviceKey) {
                return this._services[serviceKey];
            }
            Block.prototype.getServices = function() {
                return this._services;
            }
            Block.prototype.getSection = function() {
                return this._section;
            }
            Block.prototype.setSection = function(section) {
                this._section = section;
            }
            Block.prototype.clearServiceList = function() {
                 this._services.length = 0;
                 this._summaryCost = 0;
                 this._summaryDuration = 0;
            }
            Block.prototype.getSummaryCost = function(increase, areWeNeedIncrease) {
                if(areWeNeedIncrease) {
                    return this._summaryCost * (1 + increase / 100);
                } else {
                    return this._summaryCost;
                }
            }
            Block.prototype.getSummaryDuration = function(increase, areWeNeedIncrease) {
                if(areWeNeedIncrease) {
                    return this._summaryDuration * (1 + increase / 100);
                } else {
                    return this._summaryDuration;
                }
            }
            Block.prototype.addSummaryCost = function(value) {
                this._summaryCost += value;
            }
            Block.prototype.addSummaryDuration = function(value) {
                this._summaryDuration += value;
            }
            Block.prototype.subSummaryCost = function(value) {
                this._summaryCost -= value;
            }
            Block.prototype.subSummaryDuration = function(value) {
                this._summaryDuration -= value;
            }
            Block.prototype.getCountServices = function() {
                return this._services.length;
            }

    		return Block;
    	})();

    	var Service = (function() {
    		function Service(params) {
                if (params.length == 1) {
                    // TO DO
                } else if (params.length == 2) {
                    // TO DO
                } else if (params.length == 3) {
                    // TO DO
                } else if (params.length == 4) {
                    this._previousCount = 1;
                    this._count = 1;
                    this._name = params[0];
                    this._cost = params[1];
                    this._duration = params[2];
                    this._description = params[3];
                } else if (params.length == 5) {
                    this._name = params[0];
                    this._cost = params[1];
                    this._duration = params[2];
                    this._description = params[3];
                    this._count = params[4];
                    this._previousCount = 1;
                }
    			
    		}

    		Service.prototype.setCount = function(newCount) {
    			this._count = newCount;
    		}
    		Service.prototype.getCount = function() {
    			return this._count;
    		}
    		Service.prototype.getName = function() {
    			return this._name;
    		}
    		Service.prototype.getCost = function() {
    			return this._cost*this._count;
    		}
            Service.prototype.getOldCost = function() {
                return this._cost*this._previousCount;
            }
            Service.prototype.getOldDuration = function() {
                return this._duration*this._previousCount;
            }
    		Service.prototype.getDuration = function() {
    			return this._duration*this._count;
    		}
            Service.prototype.getDescription = function() {
                return this._description;
            }
            Service.prototype.setDescription = function(description) {
                this._description = description;
            }
            Service.prototype.getPreviousCount = function() {
                return this._previousCount;
            }
            Service.prototype.setPreviousCount = function(value) {
                this._previousCount = value;
            }

    		return Service;
    	})();

        return {
            getBlockInst: function() {
                return new Block();
            },
            getServiceInst: function(params) {
                return new Service(params);
            },
            getServiceInstFromJSON: function(JSON) {
                var params = [JSON.name, JSON.cost, JSON.duration, JSON.description];
                return new Service(params);
            },
            getServiceInstFromPOJO: function(POJO) {
                var params = [POJO._name, POJO._cost, POJO._duration, POJO._description, POJO._count];
                return new Service(params);
            }
        }
    }
);
