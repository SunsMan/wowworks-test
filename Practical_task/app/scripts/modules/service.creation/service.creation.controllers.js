serviceCreatorModule.controller('ServiceCreatorController', ['$scope', '$q', '$interval', '$modal', 'AssignmentsKeeper',
    function($scope, $q, $interval, $modal, AssignmentsKeeper) {
        $scope.nightIncrease = 30;
        $scope.urgentIncrease = -30;
        $scope.urgentIncreaseChecked = false;
        $scope.nightIncreaseChecked = false;

        $scope.blockList = [];
        $scope.blockList.push(AssignmentsKeeper.getBlockInst());

        $scope.selectedBlock =  0;

        $scope.blockList.areWeHaveAddedService = false;

        $scope.blockList.summaryCost = 0;
        $scope.blockList.summaryDuration = 0;

        $scope.getSummaryCost = function() {
            if ($scope.nightIncreaseChecked)
                return $scope.blockList.summaryCost * (1 + $scope.nightIncrease / 100);
            else
                return $scope.blockList.summaryCost;
        };
        $scope.setSelectedBlock = function(blockKey) {
            $scope.selectedBlock = blockKey;
        }
        $scope.getBlockClassFromSelection = function(blockKey) {
            if (blockKey == $scope.selectedBlock)
                return 'wow-service-list__block--selected';
            else 
                return 'wow-service-list__block--unselected';                            
        };
        $scope.removeService = function(blockKey, serviceKey) {
            var removedService = $scope.blockList[blockKey].getService(serviceKey);
            $scope.blockList.summaryCost -= removedService.getCost();
            $scope.blockList.summaryDuration -= removedService.getDuration();
            $scope.blockList[blockKey].deleteService(serviceKey);
        }

        $scope.getSummaryDuration = function() {
            if ($scope.urgentIncreaseChecked)
                return $scope.blockList.summaryDuration * (1 + $scope.urgentIncrease / 100);
            else
                return $scope.blockList.summaryDuration;
        };

        $scope.getModalDescription = function(description) {

        };

        $scope.addNightIncrease = function() {
            if ($scope.nightIncreaseChecked) {
                $scope.nightIncreaseChecked = false;
            } else {
                $scope.nightIncreaseChecked = true;
            }
        };
        $scope.addUrgentIncrease = function() {
            if ($scope.urgentIncreaseChecked) {
                $scope.urgentIncreaseChecked = false;
            } else {
                $scope.urgentIncreaseChecked = true;
            }
        };

        $scope.removeBlock = function(blockKey) {
            if ($scope.blockList.length > 1) {
                $scope.selectedBlock = 0;
                $scope.blockList.summaryCost -= $scope.blockList[blockKey].getSummaryCost();
                $scope.blockList.summaryDuration -= $scope.blockList[blockKey].getSummaryDuration();
                $scope.blockList.splice(blockKey, 1);
            }
        }

        $scope.serviceMoved = function(blockKey, serviceKey) {
            // $scope.blockList[blockKey].subSummaryCost(service.cost);
            // $scope.blockList[blockKey].subSummaryDuration(service.duration);
            $scope.blockList[blockKey].deleteService(serviceKey);
        };
        $scope.serviceDropped = function(service, index, blockKey) {
            $scope.blockList[blockKey].addSummaryCost(service._cost);
            $scope.blockList[blockKey].addSummaryDuration(service._duration);
            return AssignmentsKeeper.getServiceInstFromPOJO(service);
        };

        $scope.changeServiceCount = function(service, blockKey) {
            if (service.getPreviousCount() == undefined) {
                service.setPreviousCount(1);
            }
            if (service.getCount() == undefined) {
                service.setCount(1);
            }
            var costChange = service.getCost() - service.getOldCost();
            var durationChange = service.getDuration() - service.getOldDuration();

            $scope.blockList[blockKey].addSummaryCost(costChange);
            $scope.blockList[blockKey].addSummaryDuration(durationChange);

            $scope.blockList.summaryCost += costChange;
            $scope.blockList.summaryDuration += durationChange;
            service.setPreviousCount(service.getCount());
        }

        $scope.addServiceToSelectedBlock = function() {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: '/partials/modal_create_task.html',
                controller: 'ModalAddServiceController',
                size: 'lg',
                backdropClass: "fade",
                resolve: {
                    section: function() {
                        // send section of the selected block; mb so?
                        return $scope.blockList[$scope.selectedBlock].getSection();
                    }
                }
            });

            modalInstance.result.then(function(exiteInfo) {
                // add to selected block returned service
                if (exiteInfo.clearBlock) {
                    $scope.blockList[$scope.selectedBlock].clearServiceList();
                }

                if ($scope.blockList[$scope.selectedBlock].getSection() != exiteInfo.section) {
                    $scope.blockList[$scope.selectedBlock].setSection(exiteInfo.section);
                }
                $scope.blockList.areWeHaveAddedService = true;
                $scope.blockList.summaryDuration += exiteInfo.serviceObj.getDuration();
                $scope.blockList.summaryCost += exiteInfo.serviceObj.getCost(); 

                $scope.blockList[$scope.selectedBlock].setSection(exiteInfo.section);

                $scope.blockList[$scope.selectedBlock].addService(exiteInfo.serviceObj);
                
            }, function() {
                // Are we really need todo smth?
            });
        };

        $scope.addBlock = function() {
                $scope.blockList.push(AssignmentsKeeper.getBlockInst());
        }

        $scope.clearBlockList = function() {
                $scope.blockList = [];
                $scope.blockList.summaryCost = 0;
                $scope.blockList.summaryDuration = 0;
                $scope.blockList.push(AssignmentsKeeper.getBlockInst());
        }
        // $scope.blockList.push(AssignmentsKeeper.)
    }
]);
