var navbarModule = angular.module('navbar', []);
var serviceCreatorModule = angular.module('serviceCreator', ['ui.bootstrap']);
var modalAddServiceModule = angular.module('modalAddService', ['ui.bootstrap']);

var modules = angular.module('modules', [
  'navbar',
  'modalAddService',
  'serviceCreator'
]);