serviceCreatorModule.factory("AssignmentsKeeper", 
    function() {
    	var Block = (function(){
    		function Block() {
    			this._servises = [];
    			this._name = "Блок работ";
    			this._summaryCost = 0;
    			this._summaryDuration = 0;
    			this._section = "unknown";
    		}

    		Block.prototype.addService = function(service) {
                this._summaryCost += service.getCost();
                this._summaryDuration += service.getDuration();
    			this._servises.push(service);
    		}
    		Block.prototype.deleteService = function(serviceKey) {
                this._summaryCost -= this._servises[serviceKey].getCost();
                this._summaryDuration -= this._servises[serviceKey].getDuration();
    			this._servises.splice(serviceKey, 1);
    		}
            Block.prototype.getSection = function() {
                return this._section;
            }
             Block.prototype.setSection = function(section) {
                this._section = section;
            }
            Block.prototype.clearServiceList = function() {
                 this._servises.length = 0;
            }
            Block.prototype.getSummaryCost = function(increase, areWeNeedIncrease) {
                if(areWeNeedIncrease) {
                    return this._summaryCost * increase / 100;
                } else {
                    return this._summaryCost;
                }
            }
            Block.prototype.getSummaryDuration = function(increase, areWeNeedIncrease) {
                if(areWeNeedIncrease) {
                    return this._summaryDuration * increase / 100;
                } else {
                    return this._summaryDuration;
                }
            }

    		return Block;
    	})();

    	var Service = (function() {
    		function Service(params) {
                if (params.length == 1) {
                    // TO DO
                }
                if (params.length == 2) {
                    // TO DO
                }
                if (params.length == 3) {
                    // TO DO
                }
                if (params.length == 4) {
                    this._count = 1;
                    this._name = name;
                    this._cost = cost;
                    this._duration = duration;
                    this._description = description;
                }
    			
    		}

    		Service.prototype.changeCount = function(newCount) {
    			this._count = newCount;
    		}
    		Service.prototype.getCount = function() {
    			return this._count;
    		}
    		Service.prototype.getName = function() {
    			return this._name;
    		}
    		Service.prototype.getCost = function() {
    			return this._cost;
    		}
    		Service.prototype.getDuration = function() {
    			return this._duration;
    		}
            Service.prototype.getDescription = function() {
                return this._description;
            }
            Service.prototype.setDescription = function(description) {
                this._description = description;
            }

    		return Service;
    	})();

        return {
            getBLockInst: function() {
                return new Block();
            },
            getServiceInst: function(params) {
                return new Service(params);
            },
            getServiceInstFromJSON: function(JSON) {
                var params = [JSON.name, JSON.cost, JSON.duration, JSON.description];
                return new Service(params);
            }
        }
    }
);
