serviceCreatorModule.controller('ServiceCreatorController', ['$scope', '$q', '$interval', '$modal', 'AssignmentsKeeper',
    function($scope, $q, $interval, $modal, AssignmentsKeeper) {
        $scope.nightIncrease = 30;
        $scope.urgentIncrease = 30;

        $scope.getModalDescription = function(description) {

        };
        $scope.addNightIncrease = function() {
            if ($scope.nightIncreaseChecked == "checked") {
                $scope.nightIncreaseChecked = "unchecked";
            } else {
                $scope.nightIncreaseChecked = "checked";
            }
        };
        $scope.addUrgentIncrease = function() {
            if ($scope.urgentIncreaseChecked == "checked") {
                $scope.urgentIncreaseChecked = "unchecked";
            } else {
                $scope.urgentIncreaseChecked = "checked";
            }
        };
        $scope.blokList = [];

        $scope.selectedBLock =  0;

        $scope.blokList.areWeHaveAddedService = false;

        $scope.addService = function() {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: '/partials/create_task.html',
                controller: 'ModalAddServiceController',
                size: 'lg',
                backdropClass: "fade",
                resolve: {
                    section: function() {
                        // send section of the selected block; mb so?
                        return $scope.blockList[$scope.selectdBlock].getSection();
                    }
                }
            });

            modalInstance.result.then(function(serviceObj, exiteInfo) {
                // add to selected block returned service
                if (exiteInfo.clearBlock) {
                    $scope.blokList[$scope.selectedBLock].clearServiceList();
                }

                if ($scope.blockList[$scope.selectdBlock].getSection() != exiteInfo.section) {
                    $scope.blokList[$scope.selectedBLock].setSection(exiteInfo.section);
                }

                $scope.blokList[$scope.selectedBLock].setSection(exiteInfo.section);

                $scope.blokList[$scope.selectedBLock].addService(serviceObj);
                
            }, function() {
                // Are we really need todo smth?
            });
        };
        // $scope.blokList.push(AssignmentsKeeper.)
    }
]);
