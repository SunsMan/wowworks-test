modalAddServiceModule.controller('ModalAddServiceController', ['$scope', '$q', '$interval', 'ServicesKeeper', 'AssignmentsKeeper',
    function($scope, $q, $interval, ServicesKeeper, $modalInstance, section, AssignmentsKeeper) {

    	$scope.sections = {};
        $scope.sections.selected = "";

        if (section != "default")
        	$scope.sections.selected = section;
        else 
        	$scope.sections.selected = "it";
        $scope.sections.category = 0;

        $scope.sections.services = [];
		$scope.exitInfo = {};
		$scope.section.hadChoosenAnother = false;
        $scope.section.hadChoosenNew = false;

        $scope.changeSection = function() {
			$scope.exitInfo.clearBlock = true;
            $scope.section.hadChoosenNew = true;
            $scope.section.hadChoosenAnother = false;
            $scope.sections.selected = $scope.section.markedSection
            $scope.sections.services = ServicesKeeper.getService();
        }

        $scope.markAsSelected = function(markedSection) {
            $scope.section.markedSection = markedSection;
            if (!$scope.section.hadChoosenNew) {
            	if (section != "default" && markedSection != section) {
            		$scope.section.hadChoosenAnother = true;
            		// change content of this modal
            		// some operations with DOM - bad style but real usefull in this case
            	} else {
                    $scope.section.selected = markedSection;
                    $scope.sections.services = ServicesKeeper.getService($scope.sections.selected);
                    $scope.section.hadChoosenAnother = false;
                }                
            } else {
                $scope.section.selected = markedSection;
                $scope.sections.services = ServicesKeeper.getService($scope.sections.selected);
                $scope.section.hadChoosenAnother = false;
            }

        }

        $scope.selectService = function(serviceJSON) {
        	var result = AssignmentsKeeper.getServiceInstFromJSON(serviceJSON);
        	$scope.exitInfo.section = markedSection;

            $modalInstance.close(result, $scope.exitInfo);	//ToDo	must sent service-object
        };

        $scope.close = function() {
            $modalInstance.dismiss();
        };

        $cope.cancel =  function() {
            section.hadChoosenAnother = false;
        }
    }
]);
