navbarModule.directive('navbar', function () {
  return {
    restrict: 'E',
    templateUrl: '/partials/navbar.html',
    replace: true
  };
});